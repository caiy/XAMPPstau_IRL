#!/bin/bash

source /home/atlas/release_setup.sh
# get kerberos token
if [ -z ${SERVICE_PASS} ]; then
  echo "You did not set the environment variable SERVICE_PASS.\n\
Please define in the gitlab project settings/CI the secret variables SERVICE_PASS and CERN_USER."
else
  echo "${SERVICE_PASS}" | kinit ${CERN_USER}@CERN.CH
fi

url_host="gitlab.cern.ch/atlas-mpp-xampp/XAMPPstau.git"
if [ -z "${GIT_AUTHTOKEN}" ]; then
     echo "Use the login credentials to push the changes"
    git remote set-url origin https://"${CERN_USER}":"${SERVICE_PASS}"@${url_host}
else
      echo "Use the gitlab token to push the changes"
      git remote set-url origin "https://gitlab-ci-token:${GIT_AUTHTOKEN}@${url_host}"
fi  

export SERVICE_PASS=""
if [ ! -f "${TEST_FILE}" ];then
  echo "Test file not found"
  exit 1
fi
echo "Set the commit ${CI_COMMIT_SHA} as reference for future CI updates"
echo ${CI_COMMIT_SHA} > XAMPPstau/test/reference_commit.txt

echo "Upload the CI Test file"
echo "xrdcp -fr  ${TEST_FILE} root://eoshome.cern.ch//eos/user/x/xampp/ci/stau/CI_ref/histograms/${CI_COMMIT_SHA}/${PROCESS}.root"
xrdcp -fr  ${TEST_FILE} root://eoshome.cern.ch//eos/user/x/xampp/ci/stau/CI_ref/histograms/${CI_COMMIT_SHA}/${PROCESS}.root

git commit XAMPPstau/test/reference_commit.txt -m 'Updated reference commit'
git remote show origin
git push origin ${CI_COMMIT_REF_NAME} 
exit 1 # don't follow this CI any further but check the new commit instead






