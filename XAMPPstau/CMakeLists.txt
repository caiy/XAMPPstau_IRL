# $Id: CMakeLists.txt 783761 2016-11-11 16:50:45Z junggjo9 $
################################################################################
# Package: XAMPPstau
################################################################################
# Declare the package name:
atlas_subdir( XAMPPstau )
# Declare the package's dependencies:
atlas_depends_on_subdirs(
   PUBLIC
   XAMPPbase   
   RestFrames
   EventShapeVariablesCalc

   Control/AthToolSupport/AsgTools
   Event/FourMomUtils
   Event/xAOD/xAODBase
   Event/xAOD/xAODEgamma
   Event/xAOD/xAODEventInfo
   Event/xAOD/xAODJet
   Event/xAOD/xAODMissingET
   Event/xAOD/xAODMuon
   Event/xAOD/xAODTau
   Event/xAOD/xAODTracking
   Event/xAOD/xAODTruth
   Event/xAOD/xAODParticleEvent 

   PhysicsAnalysis/AnalysisCommon/PATInterfaces
   PhysicsAnalysis/AnalysisCommon/IsolationSelection
   PhysicsAnalysis/MCTruthClassifier
   PhysicsAnalysis/SUSYPhys/SUSYTools 
   
   Trigger/TrigAnalysis/TriggerMatchingTool
   Trigger/TrigConfiguration/TrigConfInterfaces
   Trigger/TrigConfiguration/TrigConfxAOD
  
   PhysicsAnalysis/Interfaces/AsgAnalysisInterfaces
   PhysicsAnalysis/Interfaces/MuonAnalysisInterfaces
   PhysicsAnalysis/Interfaces/EgammaAnalysisInterfaces
   PhysicsAnalysis/Interfaces/JetAnalysisInterfaces
   PhysicsAnalysis/TauID/TauAnalysisTools
   Tools/PathResolver)

# External dependencies:
find_package( ROOT COMPONENTS Core Tree RIO Hist Physics )


# Build a dictionary for the library:
atlas_add_root_dictionary( XAMPPstauLib _dictionarySource
   ROOT_HEADERS XAMPPstau/*.h 
   EXTERNAL_PACKAGES ROOT )
   
# Libraries in the package:
atlas_add_library( XAMPPstauLib
   XAMPPstau/*.h Root/*.cxx  ${_dictionarySource}
   PUBLIC_HEADERS XAMPPstau
   INCLUDE_DIRS  ${ROOT_INCLUDE_DIRS} ${RESTFRAMES_INCLUDE_DIRS}
   LINK_LIBRARIES ${ROOT_LIBRARIES} AsgTools xAODCore xAODEgamma xAODEventInfo  EventShapeVariablesCalcLib
   xAODJet xAODMissingET xAODMuon xAODTau xAODTracking xAODTruth SUSYToolsLib IsolationSelectionLib RestFramesLib
   XAMPPbaseLib 
   PRIVATE_LINK_LIBRARIES ${ROOT_LIBRARIES}
    PATInterfaces MCTruthClassifierLib 
   PRIVATE_LINK_LIBRARIES xAODTrigger PathResolver )


atlas_add_component( XAMPPstau
      src/*.h src/*.cxx src/components/*.cxx
      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
      LINK_LIBRARIES ${ROOT_LIBRARIES} XAMPPstauLib )

atlas_add_dictionary( XAMPPstauDict
   XAMPPstau/XAMPPstauDict.h
   XAMPPstau/selection.xml
   LINK_LIBRARIES XAMPPstauLib )

# Install files from the package:
atlas_install_data( data/* )
atlas_install_python_modules( python/*.py )
atlas_install_joboptions( share/*.py )
