# ttbar (https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/TopFocusGroup)
mc16_13TeV:mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_SUSY3.e6337_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.410471.PhPy8EG_A14_ttbar_hdamp258p75_allhad.deriv.DAOD_SUSY3.e6337_s3126_r9364_p3990

# single-top (https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/TopFocusGroup)
mc16_13TeV:mc16_13TeV.410644.PowhegPythia8EvtGen_A14_singletop_schan_lept_top.deriv.DAOD_SUSY3.e6527_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.410645.PowhegPythia8EvtGen_A14_singletop_schan_lept_antitop.deriv.DAOD_SUSY3.e6527_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.410646.PowhegPythia8EvtGen_A14_Wt_DR_inclusive_top.deriv.DAOD_SUSY3.e6552_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.410647.PowhegPythia8EvtGen_A14_Wt_DR_inclusive_antitop.deriv.DAOD_SUSY3.e6552_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.410658.PhPy8EG_A14_tchan_BW50_lept_top.deriv.DAOD_SUSY3.e6671_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.410659.PhPy8EG_A14_tchan_BW50_lept_antitop.deriv.DAOD_SUSY3.e6671_s3126_r9364_p3990

# ttV
mc16_13TeV:mc16_13TeV.410155.aMcAtNloPythia8EvtGen_MEN30NLO_A14N23LO_ttW.deriv.DAOD_SUSY3.e5070_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.410156.aMcAtNloPythia8EvtGen_MEN30NLO_A14N23LO_ttZnunu.deriv.DAOD_SUSY3.e5070_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.410157.aMcAtNloPythia8EvtGen_MEN30NLO_A14N23LO_ttZqq.deriv.DAOD_SUSY3.e5070_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.410218.aMcAtNloPythia8EvtGen_MEN30NLO_A14N23LO_ttee.deriv.DAOD_SUSY3.e5070_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.410219.aMcAtNloPythia8EvtGen_MEN30NLO_A14N23LO_ttmumu.deriv.DAOD_SUSY3.e5070_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.410220.aMcAtNloPythia8EvtGen_MEN30NLO_A14N23LO_tttautau.deriv.DAOD_SUSY3.e5070_s3126_r9364_p3990

# tt+X, tZ
mc16_13TeV:mc16_13TeV.410080.MadGraphPythia8EvtGen_A14NNPDF23_4topSM.deriv.DAOD_SUSY3.e4111_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.410081.MadGraphPythia8EvtGen_A14NNPDF23_ttbarWW.deriv.DAOD_SUSY3.e4111_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.304014.MadGraphPythia8EvtGen_A14NNPDF23_3top_SM.deriv.DAOD_SUSY3.e4324_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.407321.MadGraphPythia8EvtGen_A14NNPDF23LO_ttbarWll.deriv.DAOD_SUSY3.e5536_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.410560.MadGraphPythia8EvtGen_A14_tZ_4fl_tchan_noAllHad.deriv.DAOD_SUSY3.e5803_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.410408.aMcAtNloPythia8EvtGen_tWZ_Ztoll_minDR1.deriv.DAOD_SUSY3.e6423_s3126_r9364_p3990

# W+jets
mc16_13TeV:mc16_13TeV.308096.Sherpa_221_NNPDF30NNLO_Wenu2jets_Min_N_TChannel.deriv.DAOD_SUSY3.e5789_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.308097.Sherpa_221_NNPDF30NNLO_Wmunu2jets_Min_N_TChannel.deriv.DAOD_SUSY3.e5767_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.308098.Sherpa_221_NNPDF30NNLO_Wtaunu2jets_Min_N_TChannel.deriv.DAOD_SUSY3.e5767_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364156.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV0_70_CVetoBVeto.deriv.DAOD_SUSY3.e5340_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364157.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV0_70_CFilterBVeto.deriv.DAOD_SUSY3.e5340_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364158.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV0_70_BFilter.deriv.DAOD_SUSY3.e5340_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364159.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV70_140_CVetoBVeto.deriv.DAOD_SUSY3.e5340_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364160.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV70_140_CFilterBVeto.deriv.DAOD_SUSY3.e5340_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364161.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV70_140_BFilter.deriv.DAOD_SUSY3.e5340_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364162.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV140_280_CVetoBVeto.deriv.DAOD_SUSY3.e5340_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364163.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV140_280_CFilterBVeto.deriv.DAOD_SUSY3.e5340_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364164.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV140_280_BFilter.deriv.DAOD_SUSY3.e5340_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364165.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV280_500_CVetoBVeto.deriv.DAOD_SUSY3.e5340_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364166.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV280_500_CFilterBVeto.deriv.DAOD_SUSY3.e5340_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364167.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV280_500_BFilter.deriv.DAOD_SUSY3.e5340_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364168.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV500_1000.deriv.DAOD_SUSY3.e5340_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364169.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV1000_E_CMS.deriv.DAOD_SUSY3.e5340_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364170.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV0_70_CVetoBVeto.deriv.DAOD_SUSY3.e5340_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364171.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV0_70_CFilterBVeto.deriv.DAOD_SUSY3.e5340_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364172.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV0_70_BFilter.deriv.DAOD_SUSY3.e5340_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364173.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV70_140_CVetoBVeto.deriv.DAOD_SUSY3.e5340_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364174.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV70_140_CFilterBVeto.deriv.DAOD_SUSY3.e5340_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364175.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV70_140_BFilter.deriv.DAOD_SUSY3.e5340_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364176.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV140_280_CVetoBVeto.deriv.DAOD_SUSY3.e5340_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364177.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV140_280_CFilterBVeto.deriv.DAOD_SUSY3.e5340_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364178.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV140_280_BFilter.deriv.DAOD_SUSY3.e5340_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364179.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV280_500_CVetoBVeto.deriv.DAOD_SUSY3.e5340_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364180.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV280_500_CFilterBVeto.deriv.DAOD_SUSY3.e5340_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364181.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV280_500_BFilter.deriv.DAOD_SUSY3.e5340_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364182.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV500_1000.deriv.DAOD_SUSY3.e5340_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364183.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV1000_E_CMS.deriv.DAOD_SUSY3.e5340_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364184.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV0_70_CVetoBVeto.deriv.DAOD_SUSY3.e5340_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364185.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV0_70_CFilterBVeto.deriv.DAOD_SUSY3.e5340_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364186.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV0_70_BFilter.deriv.DAOD_SUSY3.e5340_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364187.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV70_140_CVetoBVeto.deriv.DAOD_SUSY3.e5340_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364188.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV70_140_CFilterBVeto.deriv.DAOD_SUSY3.e5340_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364189.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV70_140_BFilter.deriv.DAOD_SUSY3.e5340_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364190.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV140_280_CVetoBVeto.deriv.DAOD_SUSY3.e5340_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364191.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV140_280_CFilterBVeto.deriv.DAOD_SUSY3.e5340_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364192.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV140_280_BFilter.deriv.DAOD_SUSY3.e5340_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364193.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV280_500_CVetoBVeto.deriv.DAOD_SUSY3.e5340_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364194.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV280_500_CFilterBVeto.deriv.DAOD_SUSY3.e5340_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364195.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV280_500_BFilter.deriv.DAOD_SUSY3.e5340_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364196.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV500_1000.deriv.DAOD_SUSY3.e5340_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364197.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV1000_E_CMS.deriv.DAOD_SUSY3.e5340_s3126_r9364_p3990

# Z+jets
mc16_13TeV:mc16_13TeV.308092.Sherpa_221_NNPDF30NNLO_Zee2jets_Min_N_TChannel.deriv.DAOD_SUSY3.e5767_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.308093.Sherpa_221_NNPDF30NNLO_Zmm2jets_Min_N_TChannel.deriv.DAOD_SUSY3.e5767_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.308094.Sherpa_221_NNPDF30NNLO_Ztautau2jets_Min_N_TChannel.deriv.DAOD_SUSY3.e5767_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.308095.Sherpa_221_NNPDF30NNLO_Znunu2jets_Min_N_TChannel.deriv.DAOD_SUSY3.e5767_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364100.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_CVetoBVeto.deriv.DAOD_SUSY3.e5271_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364101.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_CFilterBVeto.deriv.DAOD_SUSY3.e5271_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364102.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_BFilter.deriv.DAOD_SUSY3.e5271_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364103.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_CVetoBVeto.deriv.DAOD_SUSY3.e5271_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364104.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_CFilterBVeto.deriv.DAOD_SUSY3.e5271_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364105.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_BFilter.deriv.DAOD_SUSY3.e5271_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364106.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV140_280_CVetoBVeto.deriv.DAOD_SUSY3.e5271_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364107.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV140_280_CFilterBVeto.deriv.DAOD_SUSY3.e5271_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364108.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV140_280_BFilter.deriv.DAOD_SUSY3.e5271_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364109.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_CVetoBVeto.deriv.DAOD_SUSY3.e5271_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364110.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_CFilterBVeto.deriv.DAOD_SUSY3.e5271_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364111.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_BFilter.deriv.DAOD_SUSY3.e5271_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364112.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV500_1000.deriv.DAOD_SUSY3.e5271_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364113.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV1000_E_CMS.deriv.DAOD_SUSY3.e5271_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364114.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV0_70_CVetoBVeto.deriv.DAOD_SUSY3.e5299_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364115.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV0_70_CFilterBVeto.deriv.DAOD_SUSY3.e5299_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364116.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV0_70_BFilter.deriv.DAOD_SUSY3.e5299_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364117.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV70_140_CVetoBVeto.deriv.DAOD_SUSY3.e5299_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364118.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV70_140_CFilterBVeto.deriv.DAOD_SUSY3.e5299_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364119.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV70_140_BFilter.deriv.DAOD_SUSY3.e5299_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364120.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV140_280_CVetoBVeto.deriv.DAOD_SUSY3.e5299_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364121.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV140_280_CFilterBVeto.deriv.DAOD_SUSY3.e5299_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364122.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV140_280_BFilter.deriv.DAOD_SUSY3.e5299_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364123.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV280_500_CVetoBVeto.deriv.DAOD_SUSY3.e5299_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364124.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV280_500_CFilterBVeto.deriv.DAOD_SUSY3.e5299_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364125.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV280_500_BFilter.deriv.DAOD_SUSY3.e5299_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364126.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV500_1000.deriv.DAOD_SUSY3.e5299_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364127.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV1000_E_CMS.deriv.DAOD_SUSY3.e5299_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364128.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV0_70_CVetoBVeto.deriv.DAOD_SUSY3.e5307_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364129.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV0_70_CFilterBVeto.deriv.DAOD_SUSY3.e5307_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364130.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV0_70_BFilter.deriv.DAOD_SUSY3.e5307_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364131.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV70_140_CVetoBVeto.deriv.DAOD_SUSY3.e5307_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364132.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV70_140_CFilterBVeto.deriv.DAOD_SUSY3.e5307_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364133.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV70_140_BFilter.deriv.DAOD_SUSY3.e5307_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364134.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV140_280_CVetoBVeto.deriv.DAOD_SUSY3.e5307_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364135.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV140_280_CFilterBVeto.deriv.DAOD_SUSY3.e5307_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364136.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV140_280_BFilter.deriv.DAOD_SUSY3.e5307_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364137.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV280_500_CVetoBVeto.deriv.DAOD_SUSY3.e5307_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364138.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV280_500_CFilterBVeto.deriv.DAOD_SUSY3.e5313_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364139.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV280_500_BFilter.deriv.DAOD_SUSY3.e5313_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364140.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV500_1000.deriv.DAOD_SUSY3.e5307_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364141.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV1000_E_CMS.deriv.DAOD_SUSY3.e5307_s3126_r9364_p3990
#mc16_13TeV:mc16_13TeV.364142.Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV0_70_CVetoBVeto.deriv.DAOD_SUSY3.e5308_s3126_r9364_p3990
#mc16_13TeV:mc16_13TeV.364143.Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV0_70_CFilterBVeto.deriv.DAOD_SUSY3.e5308_s3126_r9364_p3990
#mc16_13TeV:mc16_13TeV.364144.Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV0_70_BFilter.deriv.DAOD_SUSY3.e5308_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.366010.Sh_221_NN30NNLO_Znunu_PTV70_100_BFilter.deriv.DAOD_SUSY3.e6695_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.366011.Sh_221_NN30NNLO_Znunu_PTV100_140_MJJ0_500_BFilter.deriv.DAOD_SUSY3.e6695_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.366012.Sh_221_NN30NNLO_Znunu_PTV100_140_MJJ500_1000_BFilter.deriv.DAOD_SUSY3.e6695_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.366013.Sh_221_NN30NNLO_Znunu_PTV100_140_MJJ1000_E_CMS_BFilter.deriv.DAOD_SUSY3.e6695_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.366014.Sh_221_NN30NNLO_Znunu_PTV140_280_MJJ0_500_BFilter.deriv.DAOD_SUSY3.e6695_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.366015.Sh_221_NN30NNLO_Znunu_PTV140_280_MJJ500_1000_BFilter.deriv.DAOD_SUSY3.e6695_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.366016.Sh_221_NN30NNLO_Znunu_PTV140_280_MJJ1000_E_CMS_BFilter.deriv.DAOD_SUSY3.e6695_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.366017.Sh_221_NN30NNLO_Znunu_PTV280_500_BFilter.deriv.DAOD_SUSY3.e6695_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.366019.Sh_221_NN30NNLO_Znunu_PTV70_100_CFilterBVeto.deriv.DAOD_SUSY3.e7033_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.366020.Sh_221_NN30NNLO_Znunu_PTV100_140_MJJ0_500_CFilterBVeto.deriv.DAOD_SUSY3.e7033_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.366021.Sh_221_NN30NNLO_Znunu_PTV100_140_MJJ500_1000_CFilterBVeto.deriv.DAOD_SUSY3.e7033_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.366022.Sh_221_NN30NNLO_Znunu_PTV100_140_MJJ1000_E_CMS_CFilterBVeto.deriv.DAOD_SUSY3.e7033_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.366023.Sh_221_NN30NNLO_Znunu_PTV140_280_MJJ0_500_CFilterBVeto.deriv.DAOD_SUSY3.e7033_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.366024.Sh_221_NN30NNLO_Znunu_PTV140_280_MJJ500_1000_CFilterBVeto.deriv.DAOD_SUSY3.e7033_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.366025.Sh_221_NN30NNLO_Znunu_PTV140_280_MJJ1000_E_CMS_CFilterBVeto.deriv.DAOD_SUSY3.e7033_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.366026.Sh_221_NN30NNLO_Znunu_PTV280_500_CFilterBVeto.deriv.DAOD_SUSY3.e7033_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.366028.Sh_221_NN30NNLO_Znunu_PTV70_100_CVetoBVeto.deriv.DAOD_SUSY3.e7033_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.366029.Sh_221_NN30NNLO_Znunu_PTV100_140_MJJ0_500_CVetoBVeto.deriv.DAOD_SUSY3.e7033_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.366030.Sh_221_NN30NNLO_Znunu_PTV100_140_MJJ500_1000_CVetoBVeto.deriv.DAOD_SUSY3.e7033_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.366031.Sh_221_NN30NNLO_Znunu_PTV100_140_MJJ1000_E_CMS_CVetoBVeto.deriv.DAOD_SUSY3.e7033_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.366032.Sh_221_NN30NNLO_Znunu_PTV140_280_MJJ0_500_CVetoBVeto.deriv.DAOD_SUSY3.e7033_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.366033.Sh_221_NN30NNLO_Znunu_PTV140_280_MJJ500_1000_CVetoBVeto.deriv.DAOD_SUSY3.e7033_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.366034.Sh_221_NN30NNLO_Znunu_PTV140_280_MJJ1000_E_CMS_CVetoBVeto.deriv.DAOD_SUSY3.e7033_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.366035.Sh_221_NN30NNLO_Znunu_PTV280_500_CVetoBVeto.deriv.DAOD_SUSY3.e7033_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364222.Sherpa_221_NNPDF30NNLO_Znunu_PTV500_1000.deriv.DAOD_SUSY3.e5626_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364223.Sherpa_221_NNPDF30NNLO_Znunu_PTV1000_E_CMS.deriv.DAOD_SUSY3.e5626_s3126_r9364_p3990

# Diboson (full leptonic decay)
mc16_13TeV:mc16_13TeV.364250.Sherpa_222_NNPDF30NNLO_llll.deriv.DAOD_SUSY3.e5894_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364253.Sherpa_222_NNPDF30NNLO_lllv.deriv.DAOD_SUSY3.e5916_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364254.Sherpa_222_NNPDF30NNLO_llvv.deriv.DAOD_SUSY3.e5916_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364286.Sherpa_222_NNPDF30NNLO_llvvjj_ss_EW4.deriv.DAOD_SUSY3.e6055_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364255.Sherpa_222_NNPDF30NNLO_lvvv.deriv.DAOD_SUSY3.e5916_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.363494.Sherpa_221_NNPDF30NNLO_vvvv.deriv.DAOD_SUSY3.e5332_s3126_r9364_p3990

# Diboson (semi leptonic decay)
mc16_13TeV:mc16_13TeV.363355.Sherpa_221_NNPDF30NNLO_ZqqZvv.deriv.DAOD_SUSY3.e5525_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.363356.Sherpa_221_NNPDF30NNLO_ZqqZll.deriv.DAOD_SUSY3.e5525_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.363357.Sherpa_221_NNPDF30NNLO_WqqZvv.deriv.DAOD_SUSY3.e5525_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.363358.Sherpa_221_NNPDF30NNLO_WqqZll.deriv.DAOD_SUSY3.e5525_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.363359.Sherpa_221_NNPDF30NNLO_WpqqWmlv.deriv.DAOD_SUSY3.e5583_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.363360.Sherpa_221_NNPDF30NNLO_WplvWmqq.deriv.DAOD_SUSY3.e5983_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.363489.Sherpa_221_NNPDF30NNLO_WlvZqq.deriv.DAOD_SUSY3.e5525_s3126_r9364_p3990

# Diboson (EW VV+jj and loop-induced ggVV)
mc16_13TeV:mc16_13TeV.364283.Sherpa_222_NNPDF30NNLO_lllljj_EW6.deriv.DAOD_SUSY3.e6055_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364284.Sherpa_222_NNPDF30NNLO_lllvjj_EW6.deriv.DAOD_SUSY3.e6055_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364285.Sherpa_222_NNPDF30NNLO_llvvjj_EW6.deriv.DAOD_SUSY3.e6055_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364287.Sherpa_222_NNPDF30NNLO_llvvjj_ss_EW6.deriv.DAOD_SUSY3.e6055_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.345705.Sherpa_222_NNPDF30NNLO_ggllll_0M4l130.deriv.DAOD_SUSY3.e6213_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.345706.Sherpa_222_NNPDF30NNLO_ggllll_130M4l.deriv.DAOD_SUSY3.e6213_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.345715.Sherpa_222_NNPDF30NNLO_ggllvvInt.deriv.DAOD_SUSY3.e6525_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.345718.Sherpa_222_NNPDF30NNLO_ggllvvWW.deriv.DAOD_SUSY3.e6525_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.345723.Sherpa_222_NNPDF30NNLO_ggllvvZZ.deriv.DAOD_SUSY3.e6213_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364302.Sherpa_222_NNPDF30NNLO_ggZllZqq.deriv.DAOD_SUSY3.e6273_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364303.Sherpa_222_NNPDF30NNLO_ggZvvZqq.deriv.DAOD_SUSY3.e6273_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364304.Sherpa_222_NNPDF30NNLO_ggWmlvWpqq.deriv.DAOD_SUSY3.e6273_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364305.Sherpa_222_NNPDF30NNLO_ggWplvWmqq.deriv.DAOD_SUSY3.e6273_s3126_r9364_p3990

# Tribosons
mc16_13TeV:mc16_13TeV.407311.Sherpa_221_NNPDF30NNLO_6l0v_EW6.deriv.DAOD_SUSY3.e5473_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.407312.Sherpa_221_NNPDF30NNLO_5l1v_EW6.deriv.DAOD_SUSY3.e5473_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.407313.Sherpa_221_NNPDF30NNLO_4l2v_EW6.deriv.DAOD_SUSY3.e5473_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.407314.Sherpa_221_NNPDF30NNLO_3l3v_EW6.deriv.DAOD_SUSY3.e5473_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.407315.Sherpa_221_NNPDF30NNLO_2l4v_EW6.deriv.DAOD_SUSY3.e5655_s3126_r9364_p3990

# low mass DY
mc16_13TeV:mc16_13TeV.364198.Sherpa_221_NN30NNLO_Zmm_Mll10_40_MAXHTPTV0_70_BVeto.deriv.DAOD_SUSY3.e5421_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364199.Sherpa_221_NN30NNLO_Zmm_Mll10_40_MAXHTPTV0_70_BFilter.deriv.DAOD_SUSY3.e5421_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364200.Sherpa_221_NN30NNLO_Zmm_Mll10_40_MAXHTPTV70_280_BVeto.deriv.DAOD_SUSY3.e5421_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364201.Sherpa_221_NN30NNLO_Zmm_Mll10_40_MAXHTPTV70_280_BFilter.deriv.DAOD_SUSY3.e5421_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364202.Sherpa_221_NN30NNLO_Zmm_Mll10_40_MAXHTPTV280_E_CMS_BVeto.deriv.DAOD_SUSY3.e5421_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364203.Sherpa_221_NN30NNLO_Zmm_Mll10_40_MAXHTPTV280_E_CMS_BFilter.deriv.DAOD_SUSY3.e5421_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364204.Sherpa_221_NN30NNLO_Zee_Mll10_40_MAXHTPTV0_70_BVeto.deriv.DAOD_SUSY3.e5421_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364205.Sherpa_221_NN30NNLO_Zee_Mll10_40_MAXHTPTV0_70_BFilter.deriv.DAOD_SUSY3.e5421_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364206.Sherpa_221_NN30NNLO_Zee_Mll10_40_MAXHTPTV70_280_BVeto.deriv.DAOD_SUSY3.e5421_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364207.Sherpa_221_NN30NNLO_Zee_Mll10_40_MAXHTPTV70_280_BFilter.deriv.DAOD_SUSY3.e5421_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364208.Sherpa_221_NN30NNLO_Zee_Mll10_40_MAXHTPTV280_E_CMS_BVeto.deriv.DAOD_SUSY3.e5421_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364209.Sherpa_221_NN30NNLO_Zee_Mll10_40_MAXHTPTV280_E_CMS_BFilter.deriv.DAOD_SUSY3.e5421_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364210.Sherpa_221_NN30NNLO_Ztt_Mll10_40_MAXHTPTV0_70_BVeto.deriv.DAOD_SUSY3.e5421_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364211.Sherpa_221_NN30NNLO_Ztt_Mll10_40_MAXHTPTV0_70_BFilter.deriv.DAOD_SUSY3.e5421_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364212.Sherpa_221_NN30NNLO_Ztt_Mll10_40_MAXHTPTV70_280_BVeto.deriv.DAOD_SUSY3.e5421_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364213.Sherpa_221_NN30NNLO_Ztt_Mll10_40_MAXHTPTV70_280_BFilter.deriv.DAOD_SUSY3.e5421_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364214.Sherpa_221_NN30NNLO_Ztt_Mll10_40_MAXHTPTV280_E_CMS_BVeto.deriv.DAOD_SUSY3.e5421_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364215.Sherpa_221_NN30NNLO_Ztt_Mll10_40_MAXHTPTV280_E_CMS_BFilter.deriv.DAOD_SUSY3.e5421_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364358.Sherpa_221_NNPDF30NNLO_Zee_Mll2Ml_MAXHTPTV70_140.deriv.DAOD_SUSY3.e6544_a875_r9364_p3990
mc16_13TeV:mc16_13TeV.364359.Sherpa_221_NNPDF30NNLO_Zmumu_Mll2Ml_MAXHTPTV70_140.deriv.DAOD_SUSY3.e6544_a875_r9364_p3990
mc16_13TeV:mc16_13TeV.364360.Sherpa_221_NNPDF30NNLO_Ztautau_Mll2Ml_MAXHTPTV70_140.deriv.DAOD_SUSY3.e6544_a875_r9364_p3990
mc16_13TeV:mc16_13TeV.364361.Sherpa_221_NNPDF30NNLO_Zee_Mll2Ml_MAXHTPTV140_280.deriv.DAOD_SUSY3.e6544_a875_r9364_p3990
mc16_13TeV:mc16_13TeV.364362.Sherpa_221_NNPDF30NNLO_Zmumu_Mll2Ml_MAXHTPTV140_280.deriv.DAOD_SUSY3.e6544_a875_r9364_p3990
mc16_13TeV:mc16_13TeV.364363.Sherpa_221_NNPDF30NNLO_Ztautau_Mll2Ml_MAXHTPTV140_280.deriv.DAOD_SUSY3.e6544_a875_r9364_p3990
mc16_13TeV:mc16_13TeV.364280.Sherpa_221_NNPDF30NNLO_Zee_Mll2Ml_MAXHTPTV280_E_CMS.deriv.DAOD_SUSY3.e6037_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364281.Sherpa_221_NNPDF30NNLO_Zmumu_Mll2Ml_MAXHTPTV280_E_CMS.deriv.DAOD_SUSY3.e6037_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.364282.Sherpa_221_NNPDF30NNLO_Ztautau_Mll2Ml_MAXHTPTV280_E_CMS.deriv.DAOD_SUSY3.e6037_s3126_r9364_p3990

# Higgs samples (See overlap with multiboson samples: https://docs.google.com/spreadsheets/d/1v5JHTsmvWPBrAwBrXfLaC8ElpOy4uSSzRBWtyAg83rg/edit#gid=0)
# ttH
mc16_13TeV:mc16_13TeV.346345.PhPy8EG_A14NNPDF23_NNPDF30ME_ttH125_dilep.deriv.DAOD_SUSY3.e7148_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.346344.PhPy8EG_A14NNPDF23_NNPDF30ME_ttH125_semilep.deriv.DAOD_SUSY3.e7148_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.346343.PhPy8EG_A14NNPDF23_NNPDF30ME_ttH125_allhad.deriv.DAOD_SUSY3.e7148_s3126_r9364_p3990

# H->tautau
mc16_13TeV:mc16_13TeV.345120.PowhegPy8EG_NNLOPS_nnlo_30_ggH125_tautaul13l7.deriv.DAOD_SUSY3.e5814_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.345121.PowhegPy8EG_NNLOPS_nnlo_30_ggH125_tautaulm15hp20.deriv.DAOD_SUSY3.e5814_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.345122.PowhegPy8EG_NNLOPS_nnlo_30_ggH125_tautaulp15hm20.deriv.DAOD_SUSY3.e5814_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.345123.PowhegPy8EG_NNLOPS_nnlo_30_ggH125_tautauh30h20.deriv.DAOD_SUSY3.e5814_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.346190.PowhegPy8EG_NNPDF30_AZNLOCTEQ6L1_VBFH125_tautaul13l7.deriv.DAOD_SUSY3.e7259_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.346191.PowhegPy8EG_NNPDF30_AZNLOCTEQ6L1_VBFH125_tautaulm15hp20.deriv.DAOD_SUSY3.e7259_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.346192.PowhegPy8EG_NNPDF30_AZNLOCTEQ6L1_VBFH125_tautaulp15hm20.deriv.DAOD_SUSY3.e7259_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.346193.PowhegPy8EG_NNPDF30_AZNLOCTEQ6L1_VBFH125_tautauh30h20.deriv.DAOD_SUSY3.e7259_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.342285.Pythia8EvtGen_A14NNPDF23LO_ZH125_inc.deriv.DAOD_SUSY3.e4246_s3126_r9364_p3990
mc16_13TeV:mc16_13TeV.342284.Pythia8EvtGen_A14NNPDF23LO_WH125_inc.deriv.DAOD_SUSY3.e4246_s3126_r9364_p3990
