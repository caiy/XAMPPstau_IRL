#ifndef XAMPPStau_StauHadHadQCDAnalysisConfig_h
#define XAMPPStau_StauHadHadQCDAnalysisConfig_h
#include <XAMPPbase/AnalysisConfig.h>

namespace XAMPP {
    class StauHadHadQCDAnalysisConfig : public AnalysisConfig {
    public:
        ASG_TOOL_CLASS(StauHadHadQCDAnalysisConfig, XAMPP::IAnalysisConfig)
        StauHadHadQCDAnalysisConfig(std::string Analysis = "Staus");
        virtual ~StauHadHadQCDAnalysisConfig() = default;

    protected:
        virtual StatusCode initializeCustomCuts();
    };
}  // namespace XAMPP
#endif
