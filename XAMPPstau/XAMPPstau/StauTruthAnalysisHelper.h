#ifndef StauTruthAnalysisHelper_H
#define StauTruthAnalysisHelper_H
#include <AsgTools/AnaToolHandle.h>
#include <AsgTools/ToolHandle.h>
#include <XAMPPbase/SUSYTruthAnalysisHelper.h>
#include <XAMPPstau/StauSampleOverlap.h>

#include <fstream>
#include <iostream>
#include <sstream>

namespace XAMPP {
    class StauTruthAnalysisHelper : public SUSYTruthAnalysisHelper {
    public:
        // Create a proper constructor for Athena
        ASG_TOOL_CLASS(StauTruthAnalysisHelper, XAMPP::IAnalysisHelper)
        //
        StauTruthAnalysisHelper(const std::string& myname);

        virtual StatusCode RemoveOverlap();

        virtual ~StauTruthAnalysisHelper();

        virtual bool AcceptEvent();

    protected:
        virtual StatusCode initializeModuleContainers();
        virtual StatusCode initializeEventVariables();
        virtual StatusCode initializeLightLeptonTree();
        virtual StatusCode initializeJetTree();
        virtual StatusCode initializeTauTree();
        virtual StatusCode initializeDiLeptonTree();
        virtual StatusCode initializeInitPartTree();

        virtual bool SUSYdsid(int) const { return true; }
        virtual StatusCode ComputeEventVariables();

        unsigned int m_NumTauCandidate_OR;
        bool m_doTauBJetOR;
        bool m_mergeSamples;
        asg::AnaToolHandle<ISampleOverlap> m_smp_OR_removal;
    };
}  // namespace XAMPP
#endif
