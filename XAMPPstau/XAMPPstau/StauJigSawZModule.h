#ifndef StauJigSawZModule_H
#define StauJigSawZModule_H

#include <XAMPPstau/StauAnalysisModule.h>

// Include restframes
#include <RestFrames/RestFrames.hh>

using namespace RestFrames;

typedef std::shared_ptr<ReconstructionFrame> RecoFrame_Ptr;
typedef std::shared_ptr<VisibleRecoFrame> VisibleRecoFrame_Ptr;
typedef std::shared_ptr<InvisibleGroup> InvisibleGroup_Ptr;
typedef std::shared_ptr<Jigsaw> Jigsaw_Ptr;
typedef std::shared_ptr<CombinatoricGroup> CombGrp_Ptr;

namespace XAMPP {
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    //  Recusrive Jigsaw reconstruction, described in https://arxiv.org/abs/1705.10733       =
    //                                  Z-boson                                              =
    //                               /           \                                           =
    //                              /             \                                          =
    //                             /               \                                         =
    //                            /                 \                                        =
    //                           /                   \                                       =
    //                        Tau_{1}              Tau_{2}                                   =
    //                       /       \            /       \                                  =
    //                  lepton_{1}   nu_{1}  lepton_{2}   nu_{2}                             =
    //                                                                                       =
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

    // To explore the power of the RJ technique please consult a nicely written thesis:
    //       https://academiccommons.columbia.edu/doi/10.7916/D8QZ2P8T
    //  1) The first class of variables refers to the  H_{n,M}^{F} variables,
    //          H_{n,m}^{F} = sum_{n} abs(P_{n}^{F}) + sum_{n} abs(P_{n}^{F})
    //     which are a generalization of the H_{T} variables elaborating the full 3 momentum.
    //     The index F indicates the frame in which the momenta are evaluated. The subscripts
    //     m,n denote the number of visible and invisible degrees of fredom. If the number
    //     of total degrees is larger than n then they are comprised until n is reached.
    //     On the other hand if n is larger than the number of all particles then H is just the
    //     scalar sum of momenta in the frame F.
    // 2) p^{LAB}_{PP,z} momentum of the invariant sparticle system/ the Z-boson in the lab frame

    class StauJigSawZModule : public StauAnalysisModule {
    public:
        enum JigSawFlavour { Z = 0, W, Susy };
        enum DecayFrame {

            Lab_Frame = 1,
            // The restframe of the Z-boson or the staustau system
            PP_Frame = 1 << 1,
            // Restframe of the decaying tau or the stau
            P1_Frame = 1 << 2,
            P2_Frame = 1 << 3,
            // Generic decaying frame
            P_Frame = P1_Frame | P2_Frame,

        };
        // Create a proper constructor for Athena
        ASG_TOOL_CLASS(StauJigSawZModule, XAMPP::IAnalysisModule)
        //
        StauJigSawZModule(const std::string& myname);
        virtual StatusCode bookVariables();
        virtual StatusCode fill();

        virtual ~StauJigSawZModule();
        virtual unsigned int flavour() const;

    protected:
        xAOD::Particle* saveInFrame(const ReconstructionFrame& Frame, int pdgId, int RestFrame = DecayFrame::Lab_Frame);
        virtual RecoFrame_Ptr getRestFrame(int frame) const;

        virtual StatusCode setupRJanalysis();

    private:
        bool m_use_invismass;
        bool m_use_mindelta_m;
        bool m_store_rj_flavour;

    protected:
        // Save the JigSaw particles to the tree
        XAMPP::ParticleStorage* m_JigSaws;

    private:
        // Visible components from the final state particles
        // I.e. the hadronic taus or the electrons/muons
        VisibleRecoFrame_Ptr m_vis_Tau1;
        VisibleRecoFrame_Ptr m_vis_Tau2;

        RecoFrame_Ptr m_nu1;
        RecoFrame_Ptr m_nu2;

        RecoFrame_Ptr m_Tau1;
        RecoFrame_Ptr m_Tau2;

        RecoFrame_Ptr m_Z;

        XAMPP::Storage<float>* m_dec_Z_H01;
        XAMPP::Storage<float>* m_dec_Z_HT01;

        XAMPP::Storage<float>* m_dec_Z_H11;
        XAMPP::Storage<float>* m_dec_Z_HT11;
        XAMPP::Storage<float>* m_dec_Z_H21;
        XAMPP::Storage<float>* m_dec_Z_HT21;
        // Ratio of HT^{Z}_{1,1} / H^{Z}_{2,1}
        XAMPP::Storage<float>* m_dec_Z_R_BalTL;

        XAMPP::Storage<float>* m_dec_CMS;
        // p^{Lab}_{Z, z} / (p^{Lab}_{Z,z} + HT^{Z}_{2,1})
        XAMPP::Storage<float>* m_dec_R_BoostZ;

        // Cos theta of the decaying particle in their own rest-frames
        XAMPP::Storage<float>* m_dec_Z_cosTheta;
        XAMPP::Storage<float>* m_dec_Z_dPhiDecay;

        XAMPP::Storage<float>* m_dec_Tau1_cosTheta;
        XAMPP::Storage<float>* m_dec_Tau1_dPhiDecay;

        XAMPP::Storage<float>* m_dec_Tau2_cosTheta;
        XAMPP::Storage<float>* m_dec_Tau2_dPhiDecay;

        // Variables in the decaying particle frame
        XAMPP::Storage<float>* m_dec_Tau1_H01;
        XAMPP::Storage<float>* m_dec_Tau1_HT01;
        XAMPP::Storage<float>* m_dec_Tau1_H11;
        XAMPP::Storage<float>* m_dec_Tau1_HT11;
        XAMPP::Storage<float>* m_dec_Tau1_H21;
        XAMPP::Storage<float>* m_dec_Tau1_HT21;

    protected:
        std::shared_ptr<LabRecoFrame> m_lab_frame;

        InvisibleGroup_Ptr m_InvisGroup;
        // General JigSaws to impose on the entire decay tree
        Jigsaw_Ptr m_inivisblemass;
        Jigsaw_Ptr m_contraboost;
        Jigsaw_Ptr m_rapidity;
        Jigsaw_Ptr m_min_deltaM;
    };
}  // namespace XAMPP
#endif
