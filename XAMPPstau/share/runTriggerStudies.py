import argparse

import os
#include the common library instead of importing it. So we have full access to the globals
# Python you're a strange guy sometimes. Someimes? Alsmost the entire day
include("XAMPPstau/StauToolSetup.py")
AssembleIO()

SetupStauTriggerTool()

SetupStauAnaHelper(InitLepHadChannel=True)

#####################################################################
#       Ensure that the matching branches are written to the tree   #
#####################################################################
SetupSUSYTriggerTool().StoreMatchingInfo = True

######################################################
#       Add the taus to the MET definition           #
######################################################

# the lephad std config file
STFile = "XAMPPstau/SUSYTools_Stau_LepHad.conf"

#alternative for testing purposes
#STFile="XAMPPstau/SUSYTools_Stau.conf"
ParseCommonStauOptions(STFile=STFile, doEventShapes=True, writeBaselineSF=True, doMetSignificnace=True)

# Disable the electrons since they do not work in 21.2.6
#SetupSystematicsTool().doNoElectrons = True
#SetupSystematicsTool().doNoBtag = True
SetupSUSYJetSelector().SeparateSF = True
SetupSUSYJetSelector().ApplyBTagSF = True
SetupSUSYElectronSelector().ApplyTriggerSF = False

SingleElectronTriggers2015 = ["HLT_e24_lhmedium_L1EM20VH", "HLT_e60_lhmedium", "HLT_e120_lhloose"]

SingleElectronTriggers2016 = ["HLT_e26_lhtight_nod0_ivarloose", "HLT_e60_lhmedium_nod0", "HLT_e140_lhloose_nod0"]

#Electron Trig SF
EleTrigSFConfig = [
    "SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0"
]

#Electron Trig Matching for SF
ElectronTriggerSF = [
    "e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_OR_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0",
]

#Muon triggers
SingleMuonTriggers2015 = ["HLT_mu20_iloose_L1MU15", "HLT_mu40", "HLT_mu50"]
SingleMuonTriggers2016 = ["HLT_mu24_ivarmedium", "HLT_mu26_ivarmedium", "HLT_mu40", "HLT_mu50"]
SingleMuonTriggers2017 = ["HLT_mu26_ivarmedium", "HLT_mu50"]

# muon trig SF
MuonTriggerSF2015 = [
    "HLT_mu20_iloose_L1MU15_OR_HLT_mu40",
]

MuonTriggerSF2016 = [
    "HLT_mu24_ivarmedium_OR_HLT_mu40",
    "HLT_mu24_ivarmedium_OR_HLT_mu50",
    "HLT_mu26_ivarmedium_OR_HLT_mu50",
]

MuonTriggerSF2017 = [
    "HLT_mu26_ivarmedium_OR_HLT_mu50",
]

#tau-lepton trig
CombinedTauTriggers = [
    "HLT_mu14_ivarloose_tau35_medium1_tracktwo",
    "HLT_e17_lhmedium_nod0_tau80_medium1_tracktwo",
]
#Lepton Triggers
SingleLepTriggers = list(
    set(SingleMuonTriggers2015 + SingleMuonTriggers2016 + SingleMuonTriggers2017 + CombinedTauTriggers + SingleElectronTriggers2015 +
        SingleElectronTriggers2016))

SetupSUSYTriggerTool().Triggers = SingleLepTriggers
#SetupSUSYTriggerTool().DisableSkimming    = not TriggerSkimming

SetupSUSYElectronSelector().SFTrigger = ElectronTriggerSF
SetupSUSYElectronSelector().TriggerSFconfig = EleTrigSFConfig

SetupSUSYMuonSelector().SFTrigger2015 = MuonTriggerSF2015
SetupSUSYMuonSelector().SFTrigger2016 = MuonTriggerSF2016
#SetupSUSYMuonSelector().SFTrigger2017       = MuonTriggerSF2017
SetupAlgorithm()
