#include <XAMPPbase/AnalysisUtils.h>
#include <XAMPPstau/StauHadHadQCDAnalysisConfig.h>

namespace XAMPP {
    StauHadHadQCDAnalysisConfig::StauHadHadQCDAnalysisConfig(std::string Analysis) : AnalysisConfig(Analysis) {}

    StatusCode StauHadHadQCDAnalysisConfig::initializeCustomCuts() {
        //########################################################################
        //          Had-had QCD stream
        //########################################################################
        CutFlow HadHadQCD("tautau");

        Cut* Least1BaselineTauCut = NewSkimmingCut("N_{#tau}^{baseline}>=2", Cut::CutType::CutInt);
        if (!Least1BaselineTauCut->initialize("n_BaseTau", ">=2")) return StatusCode::FAILURE;
        HadHadQCD.push_back(Least1BaselineTauCut);

        Cut* Least1BJetCut = NewSkimmingCut("N_{#bjets}>=1", Cut::CutType::CutInt);
        if (!Least1BJetCut->initialize("n_BJets", ">=1")) return StatusCode::FAILURE;

        Cut* TCRMETCut = NewSkimmingCut("MetTST > 60 GeV", Cut::CutType::CutXAMPPmet);
        if (!TCRMETCut->initialize("MetTST", ">=60000")) return StatusCode::FAILURE;
        auto TCRCut = Least1BJetCut->combine(TCRMETCut, Cut::AND);

        Cut* BVetoCut = NewSkimmingCut("N_{#bjets}==0", Cut::CutType::CutInt);
        if (!BVetoCut->initialize("n_BJets", "==0")) return StatusCode::FAILURE;

        Cut* TwoTauCaseTau1PtCut = NewSkimmingCut("p_{T} (#tau^{0}) > 35 GeV", Cut::CutType::CutPartFloat);
        if (!TwoTauCaseTau1PtCut->initialize("taus pt[0]", ">=35000")) return StatusCode::FAILURE;

        Cut* TwoTauCaseTau2PtCut = NewSkimmingCut("p_{T} (#tau^{1}) > 25 GeV", Cut::CutType::CutPartFloat);
        if (!TwoTauCaseTau2PtCut->initialize("taus pt[1]", ">=25000")) return StatusCode::FAILURE;

        Cut* TwoTauCaseMETCut = NewSkimmingCut("MetTST > 50 GeV", Cut::CutType::CutXAMPPmet);
        if (!TwoTauCaseMETCut->initialize("MetTST", ">=50000")) return StatusCode::FAILURE;

        Cut* TwoTauCaseTau1PtAsymCut = NewSkimmingCut("p_{T} (#tau^{0}) > 80 GeV", Cut::CutType::CutPartFloat);
        if (!TwoTauCaseTau1PtAsymCut->initialize("taus pt[0]", ">=80000")) return StatusCode::FAILURE;

        Cut* TwoTauCaseTau2PtAsymCut = NewSkimmingCut("p_{T} (#tau^{1}) > 50 GeV", Cut::CutType::CutPartFloat);
        if (!TwoTauCaseTau2PtAsymCut->initialize("taus pt[1]", ">=50000")) return StatusCode::FAILURE;

        Cut* Exact2SignalTauCut = NewSkimmingCut("N_{#tau}^{signal} < 2", Cut::CutType::CutInt);
        if (!Exact2SignalTauCut->initialize("n_SignalTau", "<2")) return StatusCode::FAILURE;

        auto TwoTauCaseCombCutDitauMET = TwoTauCaseTau1PtCut->combine(TwoTauCaseTau2PtCut, Cut::AND)->combine(TwoTauCaseMETCut, Cut::AND);
        auto TwoTauCaseCombCutAsym = TwoTauCaseTau1PtAsymCut->combine(TwoTauCaseTau2PtAsymCut, Cut::AND);

        auto tauPtCuts = TwoTauCaseCombCutDitauMET->combine(TwoTauCaseCombCutAsym, Cut::OR);

        auto QCDCut = Exact2SignalTauCut->combine(tauPtCuts, Cut::AND)->combine(BVetoCut, Cut::AND);

        auto finalSlimCut = QCDCut->combine(TCRCut, Cut::OR);

        HadHadQCD.push_back(finalSlimCut);

        ATH_CHECK(AddToCutFlows(HadHadQCD));

        return StatusCode::SUCCESS;
    }
}  // namespace XAMPP
