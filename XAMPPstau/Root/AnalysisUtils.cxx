#include <MCTruthClassifier/MCTruthClassifierDefs.h>
#include <SUSYTools/SUSYObjDef_xAOD.h>
#include <XAMPPbase/EventInfo.h>
#include <XAMPPbase/ReconstructedParticles.h>
#include <XAMPPbase/ToolHandleSystematics.h>
#include <XAMPPstau/AnalysisUtils.h>

#include <fstream>
#include <iostream>
#include <sstream>

namespace XAMPP {
    TVector3 GetRestBoost(const xAOD::IParticle& Part) { return GetRestBoost(&Part); }
    TVector3 GetRestBoost(const xAOD::IParticle* Part) { return GetRestBoost(Part->p4()); }
    TVector3 GetRestBoost(TLorentzVector Vec) { return Vec.BoostVector(); }
    bool ZMassSorter(const xAOD::IParticle* p1, const xAOD::IParticle* p2) {
        return fabs(p1->m() - XAMPP::Z_MASS) < fabs(p2->m() - XAMPP::Z_MASS);
    }
    float VecSumPt(const xAOD::IParticle* v1, const xAOD::IParticle* v2) {
        if (v1 == nullptr || v2 == nullptr) return -1;
        return (v1->p4() + v2->p4()).Pt();
    }

    float SumCosDeltaPhi(const xAOD::IParticle* p1, const xAOD::IParticle* p2, const xAOD::MissingET* met) {
        if (p1 == nullptr || p2 == nullptr || met == nullptr) return -2;
        return TMath::Cos(xAOD::P4Helpers::deltaPhi(p1, met)) + TMath::Cos(xAOD::P4Helpers::deltaPhi(p2, met));
    }

    float CosMinDeltaPhi(const xAOD::IParticle* p1, const xAOD::IParticle* p2, const xAOD::MissingET* met) {
        if (p1 == nullptr || p2 == nullptr || met == nullptr) return -2;
        return TMath::Cos(std::min(xAOD::P4Helpers::deltaPhi(p1, met), xAOD::P4Helpers::deltaPhi(p2, met)));
    }

    float PairMetDeltaPhi(const xAOD::IParticle* p1, const xAOD::IParticle* p2, const xAOD::MissingET* met) {
        if (p1 == nullptr || p2 == nullptr || met == nullptr) return -2;
        return xAOD::P4Helpers::deltaPhi((p1->p4() + p2->p4()).Phi(), met->phi());
    }

    float PTt(const xAOD::IParticle* p1, const xAOD::IParticle* p2) {
        if (!p1 || !p2) return -1;

        return (p1->p4() + p2->p4()).Pt() * std::sin((p1->p4() + p2->p4()).DeltaPhi(p1->p4() - p2->p4()));
    }

    float CosChi1(const xAOD::IParticle* p1, const xAOD::IParticle* p2, const xAOD::IParticle* p3) {
        if (!p1 || !p2 || !p3) return -2;
        return (p1->p4().Vect().Unit().Cross(p2->p4().Vect().Unit())).Dot(p3->p4().Vect().Unit());
    }

    float CosChi2(const xAOD::IParticle* p1, const xAOD::IParticle* p2, const xAOD::IParticle* p3, const xAOD::IParticle* p4) {
        if (!p1 || !p2 || !p3 || !p4) return -2;
        return (p1->p4().Vect().Unit().Cross(p2->p4().Vect().Unit())).Dot(p3->p4().Vect().Unit().Cross(p4->p4().Vect().Unit()));
    }

    double METcentrality(const xAOD::IParticle* p1, const xAOD::IParticle* p2, XAMPP::Storage<xAOD::MissingET*>* met) {
        if (met->GetValue()) return METcentrality(p1, p2, met->GetValue());
        return FLT_MIN;
    }
    double METcentrality(const xAOD::IParticle* p1, const xAOD::IParticle* p2, const xAOD::MissingET* met) {
        if (!p1 || !p2 || !met) return FLT_MIN;
        double d = std::sin(xAOD::P4Helpers::deltaPhi(p2->phi(), p1->phi()));
        if (d != 0) {
            double A = std::sin(xAOD::P4Helpers::deltaPhi(met->phi(), p1->phi())) / d;
            double B = std::sin(xAOD::P4Helpers::deltaPhi(p2->phi(), met->phi())) / d;
            double AB2 = std::sqrt(A * A + B * B);
            if (AB2 > 0) return (A + B) / AB2;
        }
        return FLT_MAX;
    }

    bool METbisect(const xAOD::IParticle* p1, const xAOD::IParticle* p2, XAMPP::Storage<xAOD::MissingET*>* met) {
        return METbisect(p1, p2, met->GetValue());
    }
    bool METbisect(const xAOD::IParticle* p1, const xAOD::IParticle* p2, const xAOD::MissingET* met) {
        if (!p1 || !p2 || !met) return false;
        double dphi = xAOD::P4Helpers::deltaPhi(p2->phi(), p1->phi());
        double dphi1 = xAOD::P4Helpers::deltaPhi(met->phi(), p1->phi());
        double dphi2 = xAOD::P4Helpers::deltaPhi(met->phi(), p2->phi());
        return (std::max(dphi1, dphi2) <= dphi) && (dphi1 + dphi2 <= TMath::Pi());
    }
    float PtEffective(const xAOD::IParticle* p1, const xAOD::IParticle* p2, XAMPP::Storage<xAOD::MissingET*>* met) {
        if (met && met->GetValue()) return PtEffective(p1, p2, met->GetValue());
        return -1;
    }

    float PtEffective(const xAOD::IParticle* p1, const xAOD::IParticle* p2, const xAOD::MissingET* met) {
        if (!met) return -1;
        TLorentzVector MET_vec(met->mpx(), met->mpy(), 0, met->met());
        if (p1) MET_vec += p1->p4();
        if (p2) MET_vec += p2->p4();

        return MET_vec.Pt();
    }
    void CopyContainer(xAOD::IParticleContainer* From, xAOD::IParticleContainer* To) { CopyContainer(*From, *To); }
    void CopyContainer(xAOD::IParticleContainer& From, xAOD::IParticleContainer* To) { CopyContainer(From, *To); }
    void CopyContainer(xAOD::IParticleContainer* From, xAOD::IParticleContainer& To) { CopyContainer(*From, To); }
    void CopyContainer(xAOD::IParticleContainer& From, xAOD::IParticleContainer& To) {
        for (auto F : From) To.push_back(F);
    }
    bool isReal(const xAOD::IParticle* particle) { return particle != nullptr && isReal(*particle); }
    bool isReal(const xAOD::IParticle& particle) {
        int pdgId = fabs(TypeToPdgId(particle));
        bool passOrigin = false;
        if (pdgId == 11 || pdgId == 13 || pdgId == 15) {
            int truthOrigin = getParticleTruthOrigin(particle);
            if (truthOrigin == MCTruthPartClassifier::ParticleOrigin::SUSY)
                passOrigin = true;
            else if (truthOrigin == MCTruthPartClassifier::ParticleOrigin::top)
                passOrigin = true;  // 10
            else if (truthOrigin == MCTruthPartClassifier::ParticleOrigin::WBoson)
                passOrigin = true;  // 12
            else if (truthOrigin == MCTruthPartClassifier::ParticleOrigin::ZBoson)
                passOrigin = true;  // 13
            else if (truthOrigin == MCTruthPartClassifier::ParticleOrigin::Higgs)
                passOrigin = true;  // 14
            else if (truthOrigin == MCTruthPartClassifier::ParticleOrigin::HiggsMSSM)
                passOrigin = true;  // 15
            else if (truthOrigin == MCTruthPartClassifier::ParticleOrigin::HeavyBoson)
                passOrigin = true;  // 16
            else if (truthOrigin == MCTruthPartClassifier::ParticleOrigin::WBosonLRSM)
                passOrigin = true;  // 17
            else if (truthOrigin == MCTruthPartClassifier::ParticleOrigin::SUSY)
                passOrigin = true;  // 22
            else if (truthOrigin == MCTruthPartClassifier::ParticleOrigin::DiBoson)
                passOrigin = true;  // 43
            else if (truthOrigin == MCTruthPartClassifier::ParticleOrigin::ZorHeavyBoson)
                passOrigin = true;  // 43
        }
        if (!passOrigin) return false;
        int truthType = getParticleTruthType(particle);
        // The particle is an electron
        if (pdgId == 11) {
            if (truthType == MCTruthPartClassifier::ParticleType::GenParticle) return true;     // 21
            if (truthType == MCTruthPartClassifier::ParticleType::IsoElectron) return true;     // 1
            if (truthType == MCTruthPartClassifier::ParticleType::NonIsoElectron) return true;  // 2
            if (truthType == MCTruthPartClassifier::ParticleType::BkgElectron) return true;     // 3
        }
        // That case we've a muon piped in
        if (pdgId == 13) {
            if (truthType == MCTruthPartClassifier::ParticleType::GenParticle) return true;  // 21
            if (truthType == MCTruthPartClassifier::ParticleType::IsoMuon) return true;      // 5
            if (truthType == MCTruthPartClassifier::ParticleType::NonIsoMuon) return true;   // 6
            if (truthType == MCTruthPartClassifier::ParticleType::BkgMuon) return true;      // 7
        }
        // Finally the tau
        else if (pdgId == 15) {
            return truthType == MCTruthPartClassifier::ParticleType::IsoTau;
        }

        return false;
    }
    TLorentzVector getOriginalP4(const xAOD::TruthParticle* p) {
        if (p->isTau()) {
            static FloatAccessor acc_eta("eta_orig"), acc_phi("phi_orig"), acc_pt("pt_orig"), acc_m("m_orig");
            TLorentzVector v;
            v.SetPtEtaPhiM(acc_pt(*p), acc_eta(*p), acc_phi(*p), acc_m(*p));
            return v;
        }

        return p->p4();
    }
    //############################################################################
    //          Top tagger functions. Need to be documented somehow
    //
    //############################################################################
    double calc_mct(const xAOD::IParticle* p1, const xAOD::IParticle* p2) {
        if (p1 == nullptr || p2 == nullptr) return -1;
        double Et_p1 = sqrt(pow(p1->pt(), 2) + pow(p1->m(), 2));
        double Et_p2 = sqrt(pow(p2->pt(), 2) + pow(p2->m(), 2));
        double mct = sqrt(fabs(pow(Et_p1 + Et_p2, 2) - pow(p1->pt() - p2->pt(), 2)));
        return mct;
    }
    bool toptag0j(const xAOD::IParticle* p1, const xAOD::IParticle* p2, XAMPP::Storage<xAOD::MissingET*>* met) {
        if (met == nullptr || met->GetValue() == nullptr) return false;
        return toptag0j(p1, p2, met);
    }
    bool toptag0j(const xAOD::IParticle* p1, const xAOD::IParticle* p2, const xAOD::MissingET* met) {
        // W_MASS defined in XAMPPbase/Defs.h
        double mctll = calc_mct(p1, p2);

        double pxus = p1->pt() * cos(p1->phi()) + p2->pt() * cos(p2->phi()) + met->mpx();
        double pyus = p1->pt() * sin(p1->phi()) + p2->pt() * sin(p2->phi()) + met->mpy();
        double rr = sqrt(pow(pxus, 2) + pow(pyus, 2)) / (2. * W_MASS);
        double fact = rr + sqrt(1 + pow(rr, 2));
        return mctll < W_MASS * fact;
    }
    unsigned int getNumTopPairs(const xAOD::IParticle* p1, const xAOD::IParticle* p2, const xAOD::JetContainer* jets,
                                const xAOD::MissingET* met, unsigned int use_n_jets) {
        if (!toptag0j(p1, p2, met)) return 0;
        unsigned int npairs = 0;
        xAOD::JetContainer::const_iterator begin = jets->begin();
        xAOD::JetContainer::const_iterator end = jets->end();

        unsigned int usedjets = 0;
        for (xAOD::JetContainer::const_iterator itr1 = begin + 1; itr1 != end; ++itr1) {
            // incorporate the breaking condition by hand
            ++usedjets;
            if (usedjets >= use_n_jets) break;
            const xAOD::Jet* first_jet = (*itr1);
            for (xAOD::JetContainer::const_iterator itr2 = begin; itr2 != end; ++itr2) { const xAOD::Jet* second_jet = (*itr2); }
        }
        return npairs;
    }

}  // namespace XAMPP
