#include <SUSYTools/SUSYObjDef_xAOD.h>
#include <TruthUtils/PIDHelpers.h>
#include <XAMPPbase/AnalysisUtils.h>
#include <XAMPPbase/ToolHandleSystematics.h>
#include <XAMPPstau/AnalysisUtils.h>
#include <XAMPPstau/StauJigSawZModule.h>

namespace XAMPP {
    static FloatDecorator dec_cosTheta("CosThetaStar");
    static FloatDecorator dec_phiDecayPlane("dPhiDecayPlane");
    static UIntDecorator dec_RJFlavour("flavour");
    static UIntDecorator dec_RJ_Frame("frame");

    StauJigSawZModule::~StauJigSawZModule() {}
    StauJigSawZModule::StauJigSawZModule(const std::string& myname) :
        StauAnalysisModule(myname),
        m_use_invismass(false),
        m_use_mindelta_m(false),
        m_store_rj_flavour(false),
        m_JigSaws(nullptr),
        m_vis_Tau1(nullptr),
        m_vis_Tau2(nullptr),
        m_nu1(nullptr),
        m_nu2(nullptr),
        m_Tau1(nullptr),
        m_Tau2(nullptr),
        m_Z(nullptr),

        m_dec_Z_H01(nullptr),
        m_dec_Z_HT01(nullptr),

        m_dec_Z_H11(nullptr),
        m_dec_Z_HT11(nullptr),
        m_dec_Z_H21(nullptr),
        m_dec_Z_HT21(nullptr),

        m_dec_Z_R_BalTL(nullptr),
        m_dec_CMS(nullptr),
        m_dec_R_BoostZ(nullptr),

        m_dec_Z_cosTheta(nullptr),
        m_dec_Z_dPhiDecay(nullptr),
        m_dec_Tau1_cosTheta(nullptr),
        m_dec_Tau1_dPhiDecay(nullptr),
        m_dec_Tau2_cosTheta(nullptr),
        m_dec_Tau2_dPhiDecay(nullptr),

        m_lab_frame(nullptr),
        m_InvisGroup(nullptr) {
        declareProperty("UseInvisibleMassJigSaw", m_use_invismass);
        declareProperty("UseMinDeltaTauMass", m_use_mindelta_m);
        declareProperty("storeRJFlavour", m_store_rj_flavour);
    }
    unsigned int StauJigSawZModule::flavour() const { return JigSawFlavour::Z; }
    StatusCode StauJigSawZModule::bookVariables() {
        ATH_CHECK(retrieveContainers());
        ATH_CHECK(bookParticleStore("JigSawCandidates", m_JigSaws, true));
        ATH_CHECK(m_JigSaws->SaveVariable<int>("pdgId"));
        // Angular variables expressed in this particular frame
        ATH_CHECK(m_JigSaws->SaveVariable<float>("CosThetaStar"));
        ATH_CHECK(m_JigSaws->SaveVariable<float>("dPhiDecayPlane"));
        if (m_store_rj_flavour) ATH_CHECK(m_JigSaws->SaveVariable<unsigned int>("flavour"));
        ATH_CHECK(m_JigSaws->SaveVariable<unsigned int>("frame"));

        ATH_CHECK(newVariable("H01_Z", -1, m_dec_Z_H01));
        ATH_CHECK(newVariable("H11_Z", -1, m_dec_Z_H11));
        ATH_CHECK(newVariable("H21_Z", -1, m_dec_Z_H21));

        ATH_CHECK(newVariable("Ht01_Z", -1, m_dec_Z_HT01));
        ATH_CHECK(newVariable("Ht11_Z", -1, m_dec_Z_HT11));
        ATH_CHECK(newVariable("Ht21_Z", -1, m_dec_Z_HT21));

        ATH_CHECK(newVariable("H01_Tau1", -1, m_dec_Tau1_H01));
        ATH_CHECK(newVariable("H11_Tau1", -1, m_dec_Tau1_H11));
        ATH_CHECK(newVariable("H21_Tau1", -1, m_dec_Tau1_H21));

        ATH_CHECK(newVariable("Ht01_Tau1", -1, m_dec_Tau1_HT01));
        ATH_CHECK(newVariable("Ht11_Tau1", -1, m_dec_Tau1_HT11));
        ATH_CHECK(newVariable("Ht21_Tau1", -1, m_dec_Tau1_HT21));

        ATH_CHECK(newVariable("BalMetObj_Z", -1, m_dec_Z_R_BalTL));
        ATH_CHECK(newVariable("CMS_Mass", -1, m_dec_CMS));
        ATH_CHECK(newVariable("R_BoostZ", -2, m_dec_R_BoostZ));

        ATH_CHECK(newVariable("cosTheta_Z", -2, m_dec_Z_cosTheta));
        ATH_CHECK(newVariable("cosTheta_Tau1", -2, m_dec_Tau1_cosTheta));
        ATH_CHECK(newVariable("cosTheta_Tau2", -2, m_dec_Tau2_cosTheta));

        ATH_CHECK(newVariable("dPhiDecayPlane_Z", 10 * std::acos(-1), m_dec_Z_dPhiDecay));
        ATH_CHECK(newVariable("dPhiDecayPlane_Tau1", 10 * std::acos(-1), m_dec_Tau1_dPhiDecay));
        ATH_CHECK(newVariable("dPhiDecayPlane_Tau2", 10 * std::acos(-1), m_dec_Tau2_dPhiDecay));

        return setupRJanalysis();
    }
    StatusCode StauJigSawZModule::setupRJanalysis() {
        m_vis_Tau1 = std::make_shared<VisibleRecoFrame>("tau1_vis", "#tau_{1}^{vis}");
        m_vis_Tau2 = std::make_shared<VisibleRecoFrame>("tau2_vis", "#tau_{2}^{vis}");

        m_nu1 = std::make_shared<InvisibleRecoFrame>("nu1", "#nu_{1}");
        m_nu2 = std::make_shared<InvisibleRecoFrame>("nu2", "#nu_{2}");

        m_Tau1 = std::make_shared<DecayRecoFrame>("tau1", "#tau_{1} #to #tau_{1}^{vis}#nu_{1}");
        m_Tau2 = std::make_shared<DecayRecoFrame>("tau2", "#tau_{2} #to #tau_{2}^{vis}#nu_{2}");

        m_Tau1->AddChildFrame(*m_vis_Tau1);
        m_Tau1->AddChildFrame(*m_nu1);

        m_Tau2->AddChildFrame(*m_vis_Tau2);
        m_Tau2->AddChildFrame(*m_nu2);

        m_Z = std::make_shared<DecayRecoFrame>("Z", "Z #to #tau#tau");
        m_Z->AddChildFrame(*m_Tau1);
        m_Z->AddChildFrame(*m_Tau2);

        m_lab_frame = std::make_shared<RestFrames::LabRecoFrame>("LabFrame", "LabFrame");
        m_lab_frame->SetChildFrame(*m_Z);
        if (!m_lab_frame->InitializeTree()) return StatusCode::FAILURE;

        ATH_MSG_INFO("Decay tree successfully built");
        m_InvisGroup = std::make_shared<InvisibleGroup>("Invisible", "#nu - jigsaws");
        m_InvisGroup->AddFrame(*m_nu1);
        m_InvisGroup->AddFrame(*m_nu2);
        ATH_MSG_INFO("Invisible group built");
        // The invariant mass of the invisible system can be choosen
        // to be the smallest Lorentz invariant function of the visible Masses
        m_inivisblemass = std::make_shared<SetMassInvJigsaw>("X1_Mass", "Set M_{#nu_{1}, #nu_{2}} to minimum");

        // The rapidity of the visible system can be choosen to be equal to the rapidity of the visible
        // particles
        std::shared_ptr<SetRapidityInvJigsaw> rapidity = std::make_shared<SetRapidityInvJigsaw>("Rapidity", "Equal rapidity");
        m_rapidity = rapidity;
        rapidity->AddVisibleFrames(m_Z->GetListVisibleFrames());

        std::shared_ptr<ContraBoostInvJigsaw> contraboost = std::make_shared<ContraBoostInvJigsaw>("Z_tautau", "m_{#tau1} == m_{#tau2}");
        m_contraboost = contraboost;

        contraboost->AddVisibleFrames(m_Tau1->GetListVisibleFrames(), 0);
        contraboost->AddVisibleFrames(m_Tau2->GetListVisibleFrames(), 1);
        contraboost->AddInvisibleFrames(m_Tau1->GetListInvisibleFrames(), 0);
        contraboost->AddInvisibleFrames(m_Tau2->GetListInvisibleFrames(), 1);

        std::shared_ptr<MinMassDiffInvJigsaw> MinDeltaMt =
            std::make_shared<MinMassDiffInvJigsaw>("MinDeltaMt", "min (M_{#tau1} - M_{#tau2})", 2);
        m_min_deltaM = MinDeltaMt;

        MinDeltaMt->AddInvisibleFrame(*m_nu1, 0);
        MinDeltaMt->AddInvisibleFrame(*m_nu2, 1);
        MinDeltaMt->AddVisibleFrame(*m_vis_Tau1, 0);
        MinDeltaMt->AddVisibleFrame(*m_vis_Tau2, 1);
        MinDeltaMt->AddMassFrame(*m_vis_Tau1, 0);
        MinDeltaMt->AddMassFrame(*m_vis_Tau2, 1);

        if (m_use_invismass) { m_InvisGroup->AddJigsaw(*m_inivisblemass); }
        m_InvisGroup->AddJigsaw(*m_rapidity);
        m_InvisGroup->AddJigsaw(*m_contraboost);
        if (m_use_mindelta_m) { m_InvisGroup->AddJigsaw(*m_min_deltaM); }
        if (!m_lab_frame->InitializeAnalysis()) return StatusCode::FAILURE;

        return StatusCode::SUCCESS;
    }

    StatusCode StauJigSawZModule::fill() {
        ATH_CHECK(fillDefaultValues());
        xAOD::IParticleContainer* electrons = m_electrons->Container();
        xAOD::IParticleContainer* muons = m_muons->Container();
        xAOD::IParticleContainer* taus = m_taus->Container();

        if (!m_ParticleConstructor->HasSubContainer(full_name("JigSawParticles"))) {
            ATH_CHECK(m_ParticleConstructor->CreateSubContainer(full_name("JigSawParticles")));
        } else {
            ATH_CHECK(m_ParticleConstructor->LoadSubContainer(full_name("JigSawParticles")));
        }
        ATH_CHECK(m_JigSaws->Fill(m_ParticleConstructor->GetSubContainer(full_name("JigSawParticles"))));

        // No taus in the event. It's hard to believe that it is something interesting
        if (taus->empty()) return StatusCode::SUCCESS;
        const xAOD::IParticle* P1 = nullptr;
        const xAOD::IParticle* P2 = nullptr;
        // A pure had had event
        if (muons->empty() && electrons->empty()) {
            P1 = taus->at(0);
        } else if (muons->empty()) {
            P1 = electrons->at(0);
        } else {
            P1 = muons->at(0);
        }
        for (const auto t : *taus) {
            if (!IsSame(t, P1)) {
                P2 = t;
                break;
            }
        }
        // There are more than one muon or electron in the event or we did not found any
        // suitable tau-candidate
        if (P2 == nullptr || muons->size() > 1 || electrons->size() > 1) {
            ATH_MSG_DEBUG("Skip event because it's not part of our topology");
            return StatusCode::SUCCESS;
        }

        m_lab_frame->ClearEvent();
        // Assign the missing-transverse eneergy
        xAOD::MissingET* met = m_met->GetValue();
        TVector3 met_TLV;
        met_TLV.SetPtEtaPhi(met->met(), 0, met->phi());
        m_InvisGroup->SetLabFrameThreeVector(met_TLV);

        // Define the taus in the event
        m_vis_Tau1->SetLabFrameFourVector(P1->p4());
        m_vis_Tau2->SetLabFrameFourVector(P2->p4());

        if (!m_lab_frame->AnalyzeEvent()) {
            ATH_MSG_DEBUG("Event analysis did not work out");
            return StatusCode::SUCCESS;
        }

        xAOD::Particle* v_visTau1_Z = saveInFrame(*m_vis_Tau1, TypeToPdgId(P1), StauJigSawZModule::PP_Frame);
        xAOD::Particle* v_visTau2_Z = saveInFrame(*m_vis_Tau2, TypeToPdgId(P2), StauJigSawZModule::PP_Frame);

        xAOD::Particle* v_Nu1_Z = saveInFrame(*m_nu1, std::fabs(TypeToPdgId(P1)) + 1, StauJigSawZModule::PP_Frame);
        xAOD::Particle* v_Nu2_Z = saveInFrame(*m_nu2, std::fabs(TypeToPdgId(P2)) + 1, StauJigSawZModule::PP_Frame);

        xAOD::Particle* v_visTau1_T1 = saveInFrame(*m_vis_Tau1, TypeToPdgId(P1), StauJigSawZModule::P1_Frame);
        xAOD::Particle* v_visTau2_T2 = saveInFrame(*m_vis_Tau2, TypeToPdgId(P2), StauJigSawZModule::P2_Frame);

        xAOD::Particle* v_Nu1_T1 = saveInFrame(*m_nu1, std::fabs(TypeToPdgId(P1)) + 1, StauJigSawZModule::P1_Frame);
        xAOD::Particle* v_Nu2_T2 = saveInFrame(*m_nu2, std::fabs(TypeToPdgId(P2)) + 1, StauJigSawZModule::P2_Frame);

        xAOD::Particle* v_Z_Lab = saveInFrame(*m_Z, 23, StauJigSawZModule::Lab_Frame);

        float H01_Z = (v_Nu1_Z->p4() + v_Nu2_Z->p4()).P();
        float HT01_Z = (v_Nu1_Z->p4() + v_Nu2_Z->p4()).Pt();
        float H11_Z = (v_visTau1_Z->p4() + v_visTau2_Z->p4()).P() + H01_Z;
        float HT11_Z = (v_visTau1_Z->p4() + v_visTau2_Z->p4()).Pt() + HT01_Z;
        float H21_Z = v_visTau1_Z->p4().P() + v_visTau2_Z->p4().P() + H01_Z;
        float HT21_Z = v_visTau1_Z->pt() + v_visTau2_Z->pt() + HT01_Z;

        ATH_CHECK(m_dec_Z_H01->Store(H01_Z));
        ATH_CHECK(m_dec_Z_HT01->Store(HT01_Z));
        ATH_CHECK(m_dec_Z_H11->Store(H11_Z));
        ATH_CHECK(m_dec_Z_HT11->Store(HT11_Z));
        ATH_CHECK(m_dec_Z_H21->Store(H21_Z));
        ATH_CHECK(m_dec_Z_HT21->Store(HT21_Z));

        float H_balance = HT11_Z / H21_Z;
        ATH_CHECK(m_dec_Z_R_BalTL->Store(H_balance));

        float CMS = m_Z->GetMass();
        ATH_CHECK(m_dec_CMS->Store(CMS));

        float pZz_LAB = v_Z_Lab->p4().Z();
        float R_BoostZ = pZz_LAB / (std::fabs(pZz_LAB) + HT21_Z);
        ATH_CHECK(m_dec_R_BoostZ->Store(R_BoostZ));

        float cos_Z = m_Z->GetCosDecayAngle();
        float cos_Tau1 = m_Tau1->GetCosDecayAngle();
        float cos_Tau2 = m_Tau2->GetCosDecayAngle();

        ATH_CHECK(m_dec_Z_cosTheta->Store(cos_Z));
        ATH_CHECK(m_dec_Tau1_cosTheta->Store(cos_Tau1));
        ATH_CHECK(m_dec_Tau2_cosTheta->Store(cos_Tau2));

        float dPhi_Z = m_Z->GetDeltaPhiDecayAngle();
        float dPhi_Tau1 = m_Tau1->GetDeltaPhiDecayAngle();
        float dPhi_Tau2 = m_Tau2->GetDeltaPhiDecayAngle();

        ATH_CHECK(m_dec_Z_dPhiDecay->Store(dPhi_Z));
        ATH_CHECK(m_dec_Tau1_dPhiDecay->Store(dPhi_Tau1));
        ATH_CHECK(m_dec_Tau2_dPhiDecay->Store(dPhi_Tau2));

        float H01_Tau1 = (v_Nu1_T1->p4() + v_Nu2_T2->p4()).P();
        float HT01_Tau1 = (v_Nu1_T1->p4() + v_Nu2_T2->p4()).Pt();
        float H11_Tau1 = (v_visTau1_T1->p4() + v_visTau2_T2->p4()).P() + H01_Tau1;
        float HT11_Tau1 = (v_visTau1_T1->p4() + v_visTau2_T2->p4()).Pt() + HT01_Tau1;
        float H21_Tau1 = v_visTau1_T1->p4().P() + v_visTau2_T2->p4().P() + H01_Tau1;
        float HT21_Tau1 = v_visTau1_T1->pt() + v_visTau2_T2->pt() + HT01_Tau1;

        ATH_CHECK(m_dec_Tau1_H01->Store(H01_Tau1));
        ATH_CHECK(m_dec_Tau1_HT01->Store(HT01_Tau1));
        ATH_CHECK(m_dec_Tau1_H11->Store(H11_Tau1));
        ATH_CHECK(m_dec_Tau1_HT11->Store(HT11_Tau1));
        ATH_CHECK(m_dec_Tau1_H21->Store(H21_Tau1));
        ATH_CHECK(m_dec_Tau1_HT21->Store(HT21_Tau1));

        return StatusCode::SUCCESS;
    }

    RecoFrame_Ptr StauJigSawZModule::getRestFrame(int frame) const {
        if (frame == StauJigSawZModule::Lab_Frame) return m_lab_frame;
        if (frame == StauJigSawZModule::PP_Frame) return m_Z;
        if (frame == StauJigSawZModule::P1_Frame) return m_Tau1;
        if (frame == StauJigSawZModule::P2_Frame) return m_Tau2;
        ATH_MSG_WARNING("Could not resolve the restframe " << frame);
        return RecoFrame_Ptr();
    }
    xAOD::Particle* StauJigSawZModule::saveInFrame(const ReconstructionFrame& Frame, int pdgId, int RestFrame) {
        RecoFrame_Ptr ref_frame = getRestFrame(RestFrame);
        if (ref_frame == nullptr) return nullptr;
        xAOD::Particle* new_part = m_ParticleConstructor->CreateEmptyParticle();
        new_part->setP4(Frame.GetFourVector(*ref_frame));
        new_part->setPdgId(pdgId);
        dec_RJFlavour(*new_part) = flavour();
        dec_RJ_Frame(*new_part) = RestFrame;
        if (Frame.IsDecayFrame()) {
            dec_cosTheta(*new_part) = Frame.GetCosDecayAngle(*ref_frame);
            //     dec_phiDecayPlane(*new_part) = Frame.GetDeltaPhiDecayAngle(*ref_frame);
        } else {
            dec_cosTheta(*new_part) = 2;
            dec_phiDecayPlane(*new_part) = 10 * std::acos(-1);
        }
        return new_part;
    }

}  // namespace XAMPP
